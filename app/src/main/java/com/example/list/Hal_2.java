package com.example.list;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Hal_2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent hasil = getIntent();
        setContentView(R.layout.activity_hal_2);
        TextView nim = findViewById(R.id.hNim);
        TextView nama = findViewById(R.id.hNama);
        TextView kelas = findViewById(R.id.hKelas);
        TextView hobi = findViewById(R.id.hHobi);
        String vNim = hasil.getStringExtra("NIM");
        String vNama = hasil.getStringExtra("NAMA");
        String vKelas = hasil.getStringExtra("KELAS");
        String vHobi = hasil.getStringExtra("HOBI");
        nim.setText("NIM   : "+vNim);
        nama.setText("NAMA  : "+vNama);
        kelas.setText("KELAS : "+vKelas);
        hobi.setText("HOBI  : "+vHobi);
    }

    public void ubah(View view) {
        Intent pindah = new Intent(getApplicationContext(),Ubah.class);
        Intent ambil = getIntent();
        boolean cek = ambil.getBooleanExtra("CEK",false);
        pindah.putExtra("CEK",cek);
        startActivity(pindah);
    }
}
