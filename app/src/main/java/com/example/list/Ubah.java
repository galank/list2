package com.example.list;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Ubah extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah);
        TextView nim = findViewById(R.id.hNim);
        TextView nama = findViewById(R.id.hNama);
        TextView kelas = findViewById(R.id.hKelas);
        TextView hobi = findViewById(R.id.hHobi);
        nim.setText("NIM   : ");
        nama.setText("NAMA  : ");
        kelas.setText("KELAS : ");
        hobi.setText("HOBI  : ");
    }
    public void simpan(View view) {
        EditText uNim = findViewById(R.id.uNim);
        EditText uNama = findViewById(R.id.uNama);
        EditText uKelas = findViewById(R.id.uKelas);
        EditText uHobi = findViewById(R.id.uHobi);
        Intent simpan = new Intent(getApplicationContext(),MainActivity.class);
        simpan.putExtra("NIM",uNim.getText().toString());
        simpan.putExtra("NAMA",uNama.getText().toString());
        simpan.putExtra("KELAS",uKelas.getText().toString());
        simpan.putExtra("HOBI",uHobi.getText().toString());
        Intent ambil = getIntent();
        boolean cek = ambil.getBooleanExtra("CEK",false);
        simpan.putExtra("CEK",cek);
        startActivity(simpan);
    }
}
