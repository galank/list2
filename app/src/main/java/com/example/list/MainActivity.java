package com.example.list;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
    private ListView list;
    String nim  [] = {
        "F1D016000",
        "F1D016001",
            "F1D016002",
            "F1D016003"
    };
    String nama  [] = {
            "Ahmad_1",
            "Ahmad_2",
            "Ahmad_3",
            "Ahmad_4"
    };
    String kelas  [] = {
            "A",
            "B",
            "A",
            "C"
    };
    String hobi  [] = {
            "Membaca",
            "Menulis",
            "Olahraga",
            "Editting"
    };
    boolean cek=false;
    int nomor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list=findViewById(R.id.lView);
        Intent get = getIntent();
        cek = get.getBooleanExtra("CEK",false);
        if(cek) {
            nim[nomor] = get.getStringExtra("NIM");
            nama[nomor] = get.getStringExtra("NAMA");
            kelas[nomor] = get.getStringExtra("KELAS");
            hobi[nomor] = get.getStringExtra("HOBI");
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                MainActivity.this,
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                nim
        );

        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                cek = true;
                Intent pindah=new Intent(getApplicationContext(), Hal_2.class);
                pindah.putExtra("NIM",nim[i]);
                pindah.putExtra("NAMA",nama[i]);
                pindah.putExtra("KELAS",kelas[i]);
                pindah.putExtra("HOBI",hobi[i]);
                pindah.putExtra("CEK",cek);
                nomor=i;
                cek=false;
                startActivity(pindah);
            }
        });

    }
}
